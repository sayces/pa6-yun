export const LOGIN_ROUTE = '/login'
export const SIGNUP_ROUTE = '/signup'
export const GALLERY_ROUTE = '/gallery'
export const PROFILE_ROUTE = '/profile'
export const CALENDAR_ROUTE = '/calendar'
export const NATIVE_ROUTE = '/'


